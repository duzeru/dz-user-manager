import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: Home
  },
  {
    path: "/home",
    name: "Home",
    component: Home
  },
  {
    path: "/edit/:username/:user",
    name: "EditUser",
    component: () =>
      import(/* webpackChunkName: "user" */ "../views/EditUser.vue")
  },
  {
    path: "/new",
    name: "NewUser",
    component: () =>
      import(/* webpackChunkName: "user" */ "../views/NewUser.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;

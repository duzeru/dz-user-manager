import Vue from "vue";
import { EventBus } from "@/support/event-bus";

import Loader from "./loader.vue";

// Vue.prototype.$swal = Vue.$loader = swal;
Vue.prototype.$loader = Vue.$loader = params => {
  // let delay = params.hasOwnProperty("delay")
  //   ? params.delay
  //   : 3.5;
  if (typeof params === "string") {
    params = {
      show: true,
      message: params
    };
  }
  if (typeof params === "boolean" || typeof params === "undefined") {
    params = {
      show: false,
      message: ""
    };
  }

  if (typeof params === "object") {
    EventBus.$emit("LOADER_TRIGGER", params);
  }
};
Vue.component("dz-loader", Loader);

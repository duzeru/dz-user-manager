/* ------------------------------------------------------- */

// chfn -f "Joe Blow" jblow
// Alterar o fullname do username
// chfn -f "Fullname" username

// Pega nas definições de login, valor mai=xino e minimo para user id
// grep -E '^UID_MIN|^UID_MAX' /etc/login.defs
// UID_MIN 1000
// UID_MAX 60000

// Seleciona todos os usuarios sudo/administradores
// getent group sudo | cut -d: -f4
// nome1, nome2

// egrep -ir '^ autologin-user =' /etc/lightdm/lightdm.conf  | cut -d"=" -f2
// explicativa:
// egrep -ir ' ^ '
// está buscando todas as linhas que começam com o parâmetro entre ' aspas simples '
// /etc/lightdm/lightdm.conf
// Localização do arquivo a ser verificado.
// |
// Vai unir a saída do egrep para o tratamento com cut
// cut -d "="
// Delimitador para expressar apenas um campo antes ou depois
// -f2
// Neste caso, apresente o campo após o delimitador.

// Uso: userdel [opções] LOGIN
// Observação: userdel: -Z requires SELinux enabled kernel
// Opções:
//   -f, --force                   força remoção dos arquivos,
//                                 mesmo se não forem do usuário
//   -h, --help                    mostrar esta mensagem de ajuda e sair
//   -r, --remove                  remove o diretório pessoal e spool de mensagens
//   -R, --root CHROOT_DIR		directório para onde fazer chroot
//   -Z, --selinux-user            remover qualquer mapeamento de utilizador SELinux para o utilizador

/*
User name
Encrypted password (x represents password is stored)
User ID number (UID)
User’s group ID number (GID)
Full name
User’s home directory
User’s Login shell (default is bash shell)
*/






// Uso: usermod [opções] LOGIN

// Opções:
//   -c, --comment COMENTÁRIO      novo valor do campo GECOS
//   -d, --home DIR_PESSOAL        novo diretório de login para a nova conta de
//                                 usuário
//   -e, --expiredate DATA_EXPIRA  define data de expiração de conta para
//                                 DATA_EXPIRA
//   -f, --inactive INATIVO        define inatividade de senha após expiração
//                                 para INATIVO
//   -g, --gid GRUPO               forçar usar GRUPO como novo grupo primário
//   -G, --groups GRUPOS           nova lista de GRUPOS suplementares
//   -a, --append                  anexa o usuário para os GRUPOS suplementares
//                                 mencionados pela opção -G sem remove-lo de
//                                 outros grupos
//   -h, --help                    mostrar esta mensagem de ajuda e sair
//   -l, --login LOGIN             novo valor do nome de login
//   -L, --lock                    trava a conta de usuário
//   -m, --move-home               move o conteúdo do diretório pessoal para
//                                 a novo localização (use somente com -d)
//   -o, --non-unique              permitir usar UID duplicados (não-únicos)
//   -p, --password SENHA          usar senha criptografada para a nova senha
//   -R, --root CHROOT_DIR  directório para onde fazer chroot
//   -s, --shell SHELL             novo shell de login para a conta de usuário
//   -u, --uid UID                 novo UID para a conta de usuário
//   -U, --unlock                  destravar a conta de usuário
//   -v, --add-subuids FIRST-LAST  add range of subordinate uids
//   -V, --del-subuids FIRST-LAST  remove range of subordinate uids
//   -w, --add-subgids FIRST-LAST  add range of subordinate gids
//   -W, --del-subgids FIRST-LAST  remove range of subordinate gids
//   -Z, --selinux-user SEUSER novo mapeamento de utilizador SELinux para a conta do utilizador

// Execute o comando passwd como "ROOT", da seguinte forma:
// Exemplo:
// passwd -S -a|awk '$2 ~ /P/ {print $1}'
// O awk filtra somente os "Usable Password (P)"

// -S STATUS
// -a TODOS
// The second field indicates if the user account has a:
// Locked password (L)
// No Password (NP)
// Usable Password (P).
// FONTE : man passwd

// grep rener /etc/group|awk -F: '{ print $1 }'

// groups rener

// List All Groups
// To view all groups present on the system simply open the /etc/group file. Each line in this file represents information for one group.

// less /etc/group
// Another option is to use the getent command which displays entries from databases configured in /etc/nsswitch.conf file including the group database which we can use to query a list of all groups.

// To get a list of all groups, type the following command:

// getent group
// The output is the same as when displaying the content of the /etc/group file. If you are using LDAP for user authentication the getent will display all groups from both /etc/group file and LDAP database.

// You can also use awk or cut to print only the first field containing the name of the group:

// getent group | awk -F: '{ print $1}'
// getent group | cut -d: -f1

// const sleep = ms => {
//   return new Promise(resolve => setTimeout(resolve, ms))
// }





// Linux terminal user add
// sudo adduser myuser --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --disabled-password
// echo "myuser:password" | sudo chpasswd

// sudo adduser myuser --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --disabled-password
// echo "myuser:password" | sudo chpasswd

// adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
// [--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GROUP | --gid ID]
// [--disabled-password] [--disabled-login] [--add_extra_groups] USUÁRIO
//    Adiciona um usuário normal

// adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]
// [--gecos GECOS] [--group | --ingroup GRUPO | --gid ID] [--disabled-password]
// [--disabled-login] [--add_extra_groups] USUÁRIO
//    Adiciona um usuário de sistema

// adduser --group [--gid ID] GRUPO
// addgroup [--gid ID] GRUPO
//    Adiciona um grupo de usuário

// addgroup --system [--gid ID] GRUPO
//    Adiciona um grupo de sistema

// adduser USUÁRIO GRUPO
//    Adiciona um usuário existente a um grupo existente

// opções gerais:
//   --quiet | -q      não passa informações de processo para stdout
//   --force-badname   permite nomes de usuário que não combinam com
//                     a variável de configuração NAME_REGEX
//   --help | -h       mensagem de utilização
//   --version | -v    número de versão e copyright
//   --conf | -c FILE  usa ARQUIVO como arquivo de configuração

// A note about granting sudo account for an existing Ubuntu/Debian Linux user
// The syntax is (must run as the root user):
// # adduser {UserNameHere} sudo UserNameHere
// # adduser sai sudo UserNameHere

// Another syntax:
// usermod -aG sudo UserNameHere

// Log in as the root user and add an existing user accout named ‘sai’ to sudo group:
// usermod -aG sudo sai
// id sai

// FIXME Verificar de usuário já existe
// sudo adduser myuser --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --disabled-password
// echo "myuser:password" | sudo chpasswd
// sudo adduser username --ingroup sudo



// FIXME sudo cat /etc/gshadow (lista usuários por grupo)

// /etc/group - informações do grupo
// /etc/gshadow - informações seguras do grupo

// Não  se  deve remover o grupo primário de um grupo existente. Deve-se remover os usuários,
// antes de se remover o grupo.

// O  comando  groupdel  modifica  os  arquivos  de  contas  do  sistema,  apagando  todas as
// referências ao grupo. O grupo a ser apagado deve existir.

// Deve-se manualmente checar os sistemas de arquivos para garantir que nenhum  dos  arquivos
// permanece com o grupo ou com a sua identificação.


// groupdel - Apaga um grupo
import Vue from "vue";

import "./styles/quasar.sass";
import lang from "quasar/lang/pt-br.js";
import "@quasar/extras/roboto-font/roboto-font.css";
import "@quasar/extras/material-icons/material-icons.css";
import {
  Quasar,
  QLayout,
  QHeader,
  QDrawer,
  QPageContainer,
  QPage,
  QToolbar,
  QToolbarTitle,
  QBtn,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel,
  QSeparator,
  QCard,
  QCardSection,
  QToggle,
  QInput,
  QDialog,
  QCardActions,
  QSelect,
  QBar,
  QSpace,
  QSpinnerBall,
  QSpinnerIos,
  QBadge,
  QScrollArea,
  QOptionGroup
} from "quasar";

Vue.use(Quasar, {
  config: {},
  components: {
    QLayout,
    QHeader,
    QDrawer,
    QPageContainer,
    QPage,
    QToolbar,
    QToolbarTitle,
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel,
    QSeparator,
    QCard,
    QCardSection,
    QToggle,
    QInput,
    QDialog,
    QCardActions,
    QSelect,
    QBar,
    QSpace,
    QSpinnerBall,
    QSpinnerIos,
    QBadge,
    QScrollArea,
    QOptionGroup
  },
  directives: {},
  plugins: {},
  lang: lang
});

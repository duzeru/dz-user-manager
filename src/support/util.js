const removeLineBreak = (str, separator) => {
  separator = separator || ";";
  return str.replace(/(\r\n|\n|\r)/gm, separator);
};

const newID = () => {
  var size = 6;
  return Math.ceil(Math.random() * Math.pow(10, size));
};

const sleepAsync = duration => {
  // const delay = duration => new Promise(resolve => setTimeout(resolve, duration));
  return new Promise(resolve => setTimeout(resolve, duration));
};

const sleepSync = milliseconds => {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
};

/**
 * Executes a shell command and return it as a Promise.
 * Original: https://medium.com/@ali.dev/how-to-use-promise-with-exec-in-node-js-a39c4d7bbf77
 * @param cmd {string}
 * @return {Promise<string>}
 */
const terminal = cmd => {
  const exec = require("child_process").exec;
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      const response = {
        status: error ? null : true,
        message: stdout || stderr
      };
      resolve(response);
    });
  });
};

const now = {
  datetime: () => {
    // Função para formatar 1 em 01
    const zeroFill = n => {
      return ("0" + n).slice(-2);
    };
    // Pega o horário atual
    const now = new Date();
    // Formata a data conforme dd/mm/aaaa hh:ii:ss
    const dataHora =
      zeroFill(now.getUTCDate()) +
      "/" +
      zeroFill(now.getMonth() + 1) +
      "/" +
      now.getFullYear() +
      " " +
      zeroFill(now.getHours()) +
      ":" +
      zeroFill(now.getMinutes()) +
      ":" +
      zeroFill(now.getSeconds());
    // Exibe na tela usando a div#data-hora
    return dataHora;
  },
  date: () => {
    // Função para formatar 1 em 01
    const zeroFill = n => {
      return ("0" + n).slice(-2);
    };
    // Pega o horário atual
    const now = new Date();
    // Formata a data conforme dd/mm/aaaa hh:ii:ss
    const data =
      zeroFill(now.getUTCDate()) +
      "/" +
      zeroFill(now.getMonth() + 1) +
      "/" +
      now.getFullYear();
    // Exibe na tela usando a div#data-hora
    return data;
  },
  time: () => {
    // Função para formatar 1 em 01
    const zeroFill = n => {
      return ("0" + n).slice(-2);
    };
    // Pega o horário atual
    const now = new Date();
    // Formata a data conforme dd/mm/aaaa hh:ii:ss
    const hora =
      zeroFill(now.getHours()) +
      ":" +
      zeroFill(now.getMinutes()) +
      ":" +
      zeroFill(now.getSeconds());
    // Exibe na tela usando a div#data-hora
    return hora;
  }
};

export { now, terminal, removeLineBreak, newID, sleepAsync, sleepSync };

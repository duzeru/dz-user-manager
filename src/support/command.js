import Vue from "vue";
import i18n from "@/i18n";
import { EventBus } from "@/support/event-bus";
import { terminal, removeLineBreak } from "@/support/util";

const userExist = async username => {
  // 1 - getent passwd $user  > /dev/null
  // 2 - if getent passwd $1 > /dev/null 2>&1; then
  //       echo "yes the user exists"
  //     else
  //       echo "No, the user does not exist"
  //     fi
  // 3 - # Check if user exists
  //     if ! id -u $FS_USER > /dev/null 2>&1; then
  //         echo "The user does not exist; execute below commands to crate and try again:"
  //         echo "  root@sh1:~# adduser --home /usr/local/freeswitch/ --shell /bin/false --no-create-home --ingroup daemon --disabled-password --disabled-login $FS_USER"
  //         echo "  ..."
  //         echo "  root@sh1:~# chown freeswitch:daemon /usr/local/freeswitch/ -R"
  //         exit 1
  //     fi
  // 4 - if [ `id -u $USER_TO_CHECK 2>/dev/null || echo -1` -ge 0 ]; then
  //     echo FOUND
  //     fi
  // 5 - if [ $(getent passwd $user) ] ; then
  //       echo user $user exists
  //     else
  //       echo user $user doesn\'t exists
  //     fi
  // 6 -   if id -u "$system_user" > /dev/null 2>&1; then
  //       # Não da return, pq ainda tem que verificar se a pasta de usuário existe
  //         echo #return 0
  //       else
  //         return 1
  //       fi
  const response = await terminal(`id -u ${username}`);

  // EventBus.$emit("STATUS", {
  //   level: "secondary",
  //   type: response.status ? "success" : "error",
  //   message: response.message
  // });

  return response;
};

const runAsRoot = async () => {
  const response = await terminal(`id -u`);
  if (response.message == "0") return true;
  return;
};

const isAdmin = async username => {
  const groups = await getUserGroups(username);
  return groups.includes("sudo");
};

const addUser = async user => {
  const response = await terminal(
    `adduser ${user.username} --gecos "${user.fullname},,," --disabled-password`
  );

  EventBus.$emit("STATUS", {
    level: "secondary",
    type: response.status ? "success" : "error",
    message: response.status
      ? i18n.t("commands.add-user.success")
      : i18n.t("commands.add-user.error", {
          username: user.username
        })
  });
  console.warn(response.message);
  return response;
};

const changeUsername = async (oldUsername, newUsername) => {
  const response = await terminal(`usermod -l ${newUsername} ${oldUsername}`);

  EventBus.$emit("STATUS", {
    level: "secondary",
    type: response.status ? "success" : "error",
    message: response.status
      ? i18n.t("commands.change-username.success")
      : i18n.t("commands.change-username.error")
  });
  console.warn(response.message);
  return response;
};

const setPassword = async (username, password) => {
  const response = await terminal(
    `echo "${username}:${password}" | sudo chpasswd`
  );

  EventBus.$emit("STATUS", {
    level: "secondary",
    type: response.status ? "success" : "warning",
    message: response.status
      ? i18n.t("commands.set-password.success")
      : i18n.t("commands.set-password.error")
  });
  console.warn(response.message);
  return response;
};

const updateGecos = async (username, gecos) => {
  const response = await terminal(
    `usermod -c "${gecos}" ${username}`
    // sudo usermod -c "Mary Carol Quinn,405,5559654,555-7704,Linux Advocate" mary
  );

  EventBus.$emit("STATUS", {
    level: "secondary",
    type: response.status ? "success" : "warning",
    message: response.status
      ? i18n.t("commands.update-gecos.success")
      : i18n.t("commands.update-gecos.error")
  });
  console.warn(response.message);
  return response;
};
const updatePassword = async (username, password) => {
  const response = await terminal(
    `echo "${username}:${password}" | sudo chpasswd`
  );

  EventBus.$emit("STATUS", {
    level: "secondary",
    type: response.status ? "success" : "warning",
    message: response.status
      ? i18n.t("commands.update-password.success")
      : i18n.t("commands.update-password.error")
  });
  console.warn(response.message);
  return response;
};

const setAdmin = async username => {
  const response = await terminal(`usermod -aG sudo ${username}`);

  EventBus.$emit("STATUS", {
    level: "secondary",
    type: response.status ? "success" : "warning",
    message: response.status
      ? i18n.t("commands.set-admin.success")
      : i18n.t("commands.set-admin.error")
  });
  console.warn(response.message);
  return response;
};

const removeAdmin = async username => {
  // Administrador = grupos (admin/sudo)
  const response = await terminal(`deluser ${username} sudo `);

  EventBus.$emit("STATUS", {
    level: "secondary",
    type: response.status ? "success" : "warning",
    message: response.status
      ? i18n.t("commands.remove-admin.success")
      : i18n.t("commands.remove-admin.error")
  });
  console.warn(response.message);
  return response;
};

const getGroups = async () => {
  const response = await terminal("getent group | cut -d: -f1");
  if (!response.status) {
    throw i18n.t("commands.get-groups.error");
  }
  var data = removeLineBreak(response.message);
  data = data.split(";");
  // O ultimo item sempre vem em branco
  // O método pop() remove o último elemento de um array e retorna aquele elemento.
  if (data.length > 1) {
    if (data[data.length - 1].trim() === "") {
      data.pop();
    }
  }
  data = data.sort();
  return data;
};

const getUserGroups = async username => {
  // tem username que aparece com groups username, mas nao aparece com este comando
  const response = await terminal(`groups ${username} | cut -d: -f2`);
  // `grep ${username} /etc/group|awk -F: '{ print $1 }'` => usa like
  if (!response.status) {
    throw i18n.t("commands.get-user-groups.error");
  }
  // usado com grep
  // var data = removeLineBreak(response.message);
  // data = data.trim().split(";");
  var data = response.message;
  data = data.trim().split(" ");
  // O ultimo item sempre vem em branco
  // O método pop() remove o último elemento de um array e retorna aquele elemento.
  if (data.length > 1) {
    if (data[data.length - 1].trim() === "") {
      data.pop();
    }
  }
  data.sort();
  return data;
};

const updateUserGroups = async (username, oldGroups, newGroups) => {
  let hasError;
  const groupForDelete = oldGroups.filter((value, index, arr) => {
    return !newGroups.includes(value);
  });
  for (const group of groupForDelete) {
    const response = await terminal(`deluser ${username} ${group}`);
    hasError = !response.status ? response.status : hasError;
    let userAndGroup = {
      username: username,
      group: group
    };
    EventBus.$emit("STATUS", {
      level: "secondary",
      type: response.status ? "success" : "warning",
      message: response.status
        ? i18n.t(
            "commands.update-user-groups.remove.success",
            userAndGroup
          )
        : i18n.t("commands.update-user-groups.remove.error", userAndGroup)
    });
  }
  const groupForInsert = newGroups.filter((value, index, arr) => {
    return !oldGroups.includes(value);
  });
  for (const group of groupForInsert) {
    const response = await terminal(`adduser ${username} ${group}`);
    hasError = !response.status ? response.status : hasError;
    let userAndGroup = {
      username: username,
      group: group
    };
    EventBus.$emit("STATUS", {
      level: "secondary",
      type: response.status ? "success" : "warning",
      message: response.status
        ? i18n.t("commands.update-user-groups.add.success", userAndGroup)
        : i18n.t("commands.update-user-groups.add.error", userAndGroup)
    });
  }
  return hasError;
};

const getUsers = async () => {
  const response = await terminal("cat /etc/passwd");
  if (!response.status) {
    throw i18n.t("commands.get-users.error")
  }
  const data = removeLineBreak(response.message);
  const userLines = data.trim().split(";");
  // data = Array.isArray(data) ? data[0].split(",") : [data];
  // O ultimo item sempre vem em branco
  // O método pop() remove o último elemento de um array e retorna aquele elemento.
  if (userLines.length > 1) {
    if (userLines[userLines.length - 1].trim() === "") {
      userLines.pop();
    }
  }

  const adminUsers = await getAdminUsers();

  const users = [];
  let i;
  for (i = 0; i < userLines.length; i++) {
    const list = userLines[i].split(":");
    if (list[2] < 1000 || list[2] > 6000) continue;
    if (!list[0]) continue;
    // gecos
    const gecos = list[4].split(",");
    const fullName = gecos[0];
    const user = {
      is_admin: adminUsers.includes(list[0]),
      account: adminUsers.includes(list[0]) ? "admin" : "default",
      username: list[0],
      // encrypted_password: list[1],
      password: "", // list[1],
      user_id: list[2],
      group_id: list[3],
      fullname: fullName,
      user_directory: list[5],
      user_shell: list[6]
    };
    users.push(user);
  }

  // 1 - ordena por account
  // 2 - ordena por username
  users.sort(function(a, b) {
    return (
      a.account.localeCompare(b.account) || a.username.localeCompare(b.username)
    );
  });
  // users.sort(function (a, b) {
  // return a.toLowerCase().localeCompare(b.toLowerCase());
  // });

  return users;
};

const getAdminUsers = async () => {
  const response = await terminal("getent group sudo | cut -d: -f4");
  if (!response.status) {
    throw i18n.t("commands.get-admin-users.error")
  }
  var data = removeLineBreak(response.message);
  data = data.trim().split(";");
  data = Array.isArray(data) ? data[0].split(",") : [data];
  // O ultimo item sempre vem em branco
  // O método pop() remove o último elemento de um array e retorna aquele elemento.
  if (data.length > 1) {
    if (data[data.length - 1].trim() === "") {
      data.pop();
    }
  }
  data.sort();
  return data;
};

export {
  runAsRoot,
  userExist,
  isAdmin,
  addUser,
  changeUsername,
  setPassword,
  updateGecos,
  updatePassword,
  setAdmin,
  removeAdmin,
  getGroups,
  getUserGroups,
  updateUserGroups,
  getAdminUsers,
  getUsers
};

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import i18n from "./i18n";

import "@/components/dz/loader";
import "./quasar";
import { EventBus } from "@/support/event-bus";

import "@/styles/app/app.min.css";

import "@/assets/lib/dz-alert/dz-alert.js";

window.onerror = function(msg, url, line) {
  EventBus.$emit("STATUS", {
    level: "primary",
    type: "error",
    message: "System error"
  });
  EventBus.$emit("STATUS", {
    level: "secondary",
    type: "error",
    message: "Message: " + msg + "=> " + line + " => " + url
  });
};

Vue.config.productionTip = false;
new Vue({
  router,
  i18n,
  data: {
    distroName: "DuZeru",
    // appName: ""
    /*
     * A.B.C {
     *  A: {
     *    nome: release maior
     *    diferença: nova funções de grande importancia
     *  }
     *  B: {
     *    nome: release menor
     *    diferença: melhorias ou evoluções
     *  }
     *  C: {
     *    nome: release de revisão
     *    diferença: correção de bugs
     *  }
     * }
     * alpha
     * beta
     * RC (release candidate)
     *
     */
    version: "0.1.0-alpha"
  },
  render: h => h(App)
}).$mount("#app");

import { EventBus } from "@/support/event-bus";
import { sleepAsync, sleepSync } from "@/support/util";
import {
  changeUsername,
  updatePassword,
  updateGecos,
  isAdmin,
  setAdmin,
  removeAdmin,
  updateUserGroups
} from "@/support/command";

/* Tempo mínimo de delay pra rodar o loader */
const tempoMinimo = 1000;

export default {
  methods: {
    async updateUser(user) {
      this.$loader(this.$t("edit-user.loader"));
      EventBus.$emit("STATUS", {
        level: "primary",
        type: "info",
        message: this.$t("edit-user.statusbar.start")
      });
      const tempoInicial = new Date().getTime();
      let hasError;

      /* ALTERA NOME COMPLETO (GECOS) */
      if (user.fullname !== this.formClone.fullname) {
        const gecos = `${user.fullname},,,`;
        const updateGecosResponse = await updateGecos(
          this.formClone.username,
          gecos
        );
        hasError = hasError || !updateGecosResponse.status;
      }

      /* ATUALIZA GRUPOS DO USUÁRIO */
      const updateUserGroupsResponse = await updateUserGroups(
        this.formClone.username,
        this.userGroupClone,
        this.userGroup
      );
      hasError = hasError || updateUserGroupsResponse;

      /* 
        Verifica se o usuario foi adicionado ao sudo (administrador)
        através de updateUserGroups
      */
      const ehAdmin = await isAdmin(this.formClone.username);

      if (!ehAdmin) {
        /* ALTERA CONTA PARA CONTA DE ADMINISTRADOR */
        if (
          user.account === "admin" &&
          user.account !== this.formClone.account
        ) {
          const setAdminResponse = await setAdmin(this.formClone.username);
          hasError = hasError || !setAdminResponse.status;
        }
      }

      if (ehAdmin) {
        /* ALTERA CONTA PARA CONTA PADRÃO */
        if (
          user.account === "default" &&
          user.account !== this.formClone.account
        ) {
          const removeAdminResponse = await removeAdmin(
            this.formClone.username
          );
          hasError = hasError || !removeAdminResponse.status;
        }
      }

      /* ALTERA A SENHA */
      if (this.showPassword) {
        const passwordResponse = await updatePassword(
          this.formClone.username,
          user.password
        );
        hasError = hasError || !passwordResponse.status;
      }

      /* ALTERA O NOME DE USUARIO */
      if (user.username !== this.formClone.username) {
        const changeUsernameResponse = await changeUsername(
          this.formClone.username,
          user.username
        );
        hasError = hasError || !changeUsernameResponse.status;
      }

      const time = tempoMinimo - (new Date().getTime() - tempoInicial);
      // sleepSync(time);
      await sleepAsync(time);
      dzAlert({
        type: hasError ? "alert" : "success",
        title: hasError
          ? this.$t("alert-title.error")
          : this.$t("alert-title.success"),
        message: hasError
          ? this.$t("edit-user.with-error")
          : this.$t("edit-user.success"),
        buttonConfirm: "Ok"
      });

      EventBus.$emit("STATUS", {
        level: "primary",
        type: hasError ? "warning" : "info",
        message: hasError
          ? this.$t("edit-user.statusbar.end-with-error")
          : this.$t("edit-user.statusbar.end")
      });

      EventBus.$emit("LOAD_USER");
      // !hasError &&
      this.$router.push("/");
      this.$loader(false);
    }
  }
};

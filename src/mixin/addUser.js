import { EventBus } from "@/support/event-bus";
import { sleepAsync, sleepSync } from "@/support/util";
import { userExist, addUser, setPassword, setAdmin } from "@/support/command";

/* Tempo mínimo de delay pra rodar o loader */
const tempoMinimo = 1000;

export default {
  methods: {
    async addUser(user) {
      this.$loader(this.$t("add-user.loader"));
      EventBus.$emit("STATUS", {
        level: "primary",
        type: "info",
        message: this.$t("add-user.statusbar.start")
      });
      const tempoInicial = new Date().getTime();
      let hasError;

      const exist = await userExist(user.username);
      hasError = hasError || exist.status;

      if (exist.status) {
        dzAlert({
          type: "alert",
          title: this.$t("alert-title.alert"),
          message: this.$t("add-user.user-exist", { username: user.username }),
          buttonConfirm: "Ok"
        });
        EventBus.$emit("STATUS", {
          level: "secondary",
          type: "warning",
          message: this.$t("add-user.user-exist", { username: user.username })
        });
      }

      if (!exist.status) {
        const newUserResponse = await addUser(user);
        hasError = hasError || !newUserResponse.status;

        if (newUserResponse.status) {
          const passwordResponse = await setPassword(
            user.username,
            user.password
          );
          hasError = hasError || !passwordResponse.status;

          if (user.account === "admin") {
            const adminResponse = await setAdmin(user.username);
            hasError = hasError || !adminResponse.status;
          }
        }

        const time = 3000; //tempoMinimo - (new Date().getTime() - tempoInicial);
        // sleepSync(time);
        // await sleepAsync(time);
        await new Promise(r => setTimeout(r, time));

        dzAlert({
          type: hasError ? "alert" : "success",
          title: hasError
            ? this.$t("alert-title.error")
            : this.$t("alert-title.success"),
          message: hasError
            ? this.$t("add-user.with-error")
            : this.$t("add-user.success"),
          buttonConfirm: "Ok"
        });
      }

      EventBus.$emit("STATUS", {
        level: "primary",
        type: hasError ? "warning" : "info",
        message: hasError
          ? this.$t("add-user.statusbar.end-with-error")
          : this.$t("add-user.statusbar.end")
      });
      EventBus.$emit("LOAD_USER");
      this.$loader(false);
    }
  }
};

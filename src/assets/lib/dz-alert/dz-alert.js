"use strict";

var createDom = function(type) {
  var el = document.createElement(type);

  var obj = {
    attr(attr) {
      for (var key in attr) {
        el.setAttribute(key, attr[key]);
      }
      return this;
    },

    parent(parent, wrap) {
      wrap = wrap ? document.querySelector(wrap) : document;
      parent = wrap.querySelector(parent);
      parent.appendChild(el);
      return this;
    },

    html(html) {
      el.innerHTML = html;
      return this;
    },

    getEl() {
      return el;
    }
  };

  return obj;
};

const iconInfo = `
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,11a1,1,0,0,0-1,1v4a1,1,0,0,0,2,0V12A1,1,0,0,0,12,11Zm.38-3.92a1,1,0,0,0-.76,0,1,1,0,0,0-.33.21,1.15,1.15,0,0,0-.21.33A.84.84,0,0,0,11,8a1,1,0,0,0,.29.71,1.15,1.15,0,0,0,.33.21A1,1,0,0,0,13,8a1.05,1.05,0,0,0-.29-.71A1,1,0,0,0,12.38,7.08ZM12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Z" />
                  </svg>
                `;

const iconQuestion = `
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <path d="M11.29,15.29a1.58,1.58,0,0,0-.12.15.76.76,0,0,0-.09.18.64.64,0,0,0-.06.18,1.36,1.36,0,0,0,0,.2.84.84,0,0,0,.08.38.9.9,0,0,0,.54.54.94.94,0,0,0,.76,0,.9.9,0,0,0,.54-.54A1,1,0,0,0,13,16a1,1,0,0,0-.29-.71A1,1,0,0,0,11.29,15.29ZM12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20ZM12,7A3,3,0,0,0,9.4,8.5a1,1,0,1,0,1.73,1A1,1,0,0,1,12,9a1,1,0,0,1,0,2,1,1,0,0,0-1,1v1a1,1,0,0,0,2,0v-.18A3,3,0,0,0,12,7Z" />
                </svg>
              `;

const iconAlert = `
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M12,16a1,1,0,1,0,1,1A1,1,0,0,0,12,16Zm10.67,1.47-8.05-14a3,3,0,0,0-5.24,0l-8,14A3,3,0,0,0,3.94,22H20.06a3,3,0,0,0,2.61-4.53Zm-1.73,2a1,1,0,0,1-.88.51H3.94a1,1,0,0,1-.88-.51,1,1,0,0,1,0-1l8-14a1,1,0,0,1,1.78,0l8.05,14A1,1,0,0,1,20.94,19.49ZM12,8a1,1,0,0,0-1,1v4a1,1,0,0,0,2,0V9A1,1,0,0,0,12,8Z" />
              </svg>
            `;

const iconError = `
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path d="M15.71,8.29a1,1,0,0,0-1.42,0L12,10.59,9.71,8.29A1,1,0,0,0,8.29,9.71L10.59,12l-2.3,2.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0L12,13.41l2.29,2.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L13.41,12l2.3-2.29A1,1,0,0,0,15.71,8.29Zm3.36-3.36A10,10,0,1,0,4.93,19.07,10,10,0,1,0,19.07,4.93ZM17.66,17.66A8,8,0,1,1,20,12,7.95,7.95,0,0,1,17.66,17.66Z" />
            </svg>
          `;

const iconSuccess = `
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <path d="M14.72,8.79l-4.29,4.3L8.78,11.44a1,1,0,1,0-1.41,1.41l2.35,2.36a1,1,0,0,0,.71.29,1,1,0,0,0,.7-.29l5-5a1,1,0,0,0,0-1.42A1,1,0,0,0,14.72,8.79ZM12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Z" />
          </svg>
        `;

// FIXME Remover uso de target

(function() {
  window.dzConfirm = dzConfirm;

  var template;
  var showing = false;

  function dzConfirm(config, callback) {
    if (!arguments.length) {
      throw "dzConfirm: No arguments were passed";
    }

    if (typeof config === "function") {
      callback = config;
    }

    config = getFinalConfig(config);

    var dialog = new Dialog(config, callback);
    dialog.show();
  }

  function Dialog(settings, callback) {
    var _this = this;
    var modal = getTemplate();

    var title = modal.querySelector(".dz-dialog__title");
    var yes = modal.querySelector(".dz-dialog__btn-confirm");
    var no = modal.querySelector(".dz-dialog__btn-cancel");
    var close = modal.querySelector(".dz-dialog__btn-close");
    var message = modal.querySelector(".dz-dialog__message");
    var typeIcon = modal.querySelector(".dz-dialog__type-icon");
    var window = modal.querySelector(".dz-dialog__window");

    title.innerHTML = settings.title;
    message.innerHTML = settings.message;
    yes.innerHTML = settings.buttonConfirm;
    no.innerHTML = settings.buttonCancel;

    typeIcon.innerHTML = iconQuestion;
    window.classList.add("dz-dialog--type-question");

    close.addEventListener("click", cancel);
    no.addEventListener("click", cancel);
    yes.addEventListener("click", confirm);

    function cancel(event) {
      event.preventDefault();
      _this.hide();

      if (typeof callback === "function") {
        callback(false, _this.context);
      }

      _this.setContext(undefined);
    }

    function confirm(event) {
      event.preventDefault();
      _this.hide();

      if (typeof callback === "function") {
        callback(true, _this.context);
      }

      _this.setContext(undefined);
    }

    _this.modal = modal;
  }

  Dialog.prototype.setContext = function(context) {
    this.context = context;
  };

  Dialog.prototype.show = function() {
    if (showing) {
      $warn("CustomConfir: There's already a confirm showing");
      return;
    }

    showing = true;
    document.body.appendChild(this.modal);
  };

  Dialog.prototype.hide = function() {
    showing = false;
    this.modal.remove();
  };

  function getFinalConfig(config) {
    var _defaults = {
      type: "question",
      title: "Confirmation",
      message: "",
      buttonConfirm: "Confirm",
      buttonCancel: "cancel"
    };

    if (typeof config === "string") {
      _defaults.message = config;
    } else if (typeof config === "object") {
      Object.assign(_defaults, config);
    }

    return _defaults;
  }

  function getTemplate() {
    if (!template) {
      template = createDom("div")
        .attr({
          class: "dz-dialog"
        })
        .html(
          `
        <div class="dz-dialog__window">
          <header class="dz-dialog__header">
            <span class="dz-dialog__title"></span>
            <button class="dz-dialog__btn-close">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                <path d="M 1.8128154,0.75215565 15.247844,14.187184 14.187185,15.247844 0.75215593,1.8128152 Z M 0.75215625,14.187185 14.187185,0.75215605 15.247844,1.8128158 1.8128156,15.247844 Z" />
              </svg>
            </button>
          </header>
          <div class="dz-dialog__body">
            <div class="dz-dialog__type-icon"></div>
            <div class="dz-dialog__message"></div>
          </div>
          <footer class="dz-dialog__footer">
            <button class="dz-dialog__button dz-dialog__btn-confirm"></button>
            <button class="dz-dialog__button dz-dialog__btn-cancel"></button>
          </footer>
        </div>
        `
        )
        .getEl();
    }

    return template.cloneNode(true);
  }

  function $warn() {
    if (
      typeof window.console === "object" &&
      typeof console.warn === "function"
    ) {
      console.warn.apply(console, arguments);
    }
  }
})();

(function() {
  window.dzAlert = dzAlert;

  var template;
  var showing = false;

  function dzAlert(config) {
    if (!arguments.length) {
      throw "dzAlert: No arguments were passed";
    }

    if (typeof config === "function") {
      throw "dzAlert: Datatype arguments invalid";
    }

    config = getFinalConfig(config);
    var dialog = new Dialog(config);
    dialog.show();
  }

  function Dialog(settings) {
    var _this = this;
    var modal = getTemplate();

    var title = modal.querySelector(".dz-dialog__title");
    var yes = modal.querySelector(".dz-dialog__btn-confirm");
    var message = modal.querySelector(".dz-dialog__message");
    var typeIcon = modal.querySelector(".dz-dialog__type-icon");
    var window = modal.querySelector(".dz-dialog__window");

    title.innerHTML = settings.title;
    message.innerHTML = settings.message;
    yes.innerHTML = settings.buttonConfirm;

    switch (settings.type) {
      case "error":
        typeIcon.innerHTML = iconError;
        window.classList.add("dz-dialog--type-error");
        break;
      case "alert":
        typeIcon.innerHTML = iconAlert;
        window.classList.add("dz-dialog--type-alert");
        break;
      case "success":
        typeIcon.innerHTML = iconSuccess;
        window.classList.add("dz-dialog--type-success");
        break;
      default:
        typeIcon.innerHTML = iconInfo;
        window.classList.add("dz-dialog--type-info");
        break;
    }

    yes.addEventListener("click", confirm);

    function cancel(event) {
      event.preventDefault();
      _this.hide();

      _this.setContext(undefined);
    }

    function confirm(event) {
      event.preventDefault();
      _this.hide();

      _this.setContext(undefined);
    }

    _this.modal = modal;
  }

  Dialog.prototype.setContext = function(context) {
    this.context = context;
  };

  Dialog.prototype.show = function() {
    if (showing) {
      $warn("CustomConfir: There's already a confirm showing");
      return;
    }

    showing = true;
    document.body.appendChild(this.modal);
  };

  Dialog.prototype.hide = function() {
    showing = false;
    this.modal.remove();
  };

  /*  */
  function getFinalConfig(config) {
    var _defaults = {
      type: "info",
      title: "Information",
      message: "",
      buttonConfirm: "Ok"
    };

    if (typeof config === "string") {
      _defaults.message = config;
    } else if (typeof config === "object") {
      Object.assign(_defaults, config);
    }

    return _defaults;
  }

  function getTemplate() {
    if (!template) {
      template = createDom("div")
        .attr({
          class: "dz-dialog"
        })
        .html(
          `
        <div class="dz-dialog__window">
          <header class="dz-dialog__header">
            <span class="dz-dialog__title"></span>
            <button class="dz-dialog__btn-close">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                <path d="M 1.8128154,0.75215565 15.247844,14.187184 14.187185,15.247844 0.75215593,1.8128152 Z M 0.75215625,14.187185 14.187185,0.75215605 15.247844,1.8128158 1.8128156,15.247844 Z" />
              </svg>
            </button>
          </header>
          <div class="dz-dialog__body">
            <div class="dz-dialog__type-icon"></div>
            <div class="dz-dialog__message"></div>
          </div>
          <footer class="dz-dialog__footer">
            <button class="dz-dialog__button  dz-dialog__btn-confirm"></button>
          </footer>
        </div>
        `
        )
        .getEl();
    }

    return template.cloneNode(true);
  }

  function $warn() {
    if (
      typeof window.console === "object" &&
      typeof console.warn === "function"
    ) {
      console.warn.apply(console, arguments);
    }
  }
})();

// /* REPLACE NATIVE ALERT/CONFIRM */

// /* ALERT */
// // keep default js alert to use in specific cases
// window.legacyAlert = window.alert;

// // types alert and confirm: "success", "error", "warning", "info", "question". Default: "warning"
// // overwrite default js alert
// window.alert = function(config) {
//   dzAlert(config);
// };

// // If you using SweetAlert2 (Swal) and JQuery you can replace all alert and confirm that way.
// // https://limonte.github.io/sweetalert2/
// // TIP: if you aren't using JQuery, use native JavaScript to create extend method. As bellow:
// function extend(a, b) {
//   for (var key in b) if (b.hasOwnProperty(key)) a[key] = b[key];
//   return a;
// }

// // // keep default js alert to use in specific cases
// // window.legacyConfirm = window.confirm;

// window.confirm = function(params) {
//   dzConfirm(params, function(confirmed, target_element) {
//     if (confirmed && func_if_yes instanceof Function) {
//       func_if_yes();
//     } else {
//       // do something (or not) when he cancels
//       func_if_cancel();
//       return;
//     }
//   });
//     swal($.extend({
//                     title: title,
//                     text: msg,
//                     type: type,
//                     showCancelButton: true,
//                     cancelButtonText: "Cancelar",
//                     confirmButtonText: "Ok",
//                     allowEscapeKey: false,
//                     allowOutsideClick: false
//                 }, params || {})
//     ).then(function(isConfirm) {
//         if (isConfirm && func_if_yes instanceof Function){
//             func_if_yes();
//         }
//     }, function(dismiss) {
//         // dismiss can be 'cancel', 'overlay', 'close', 'timer'
//         if (dismiss === 'cancel' && func_if_cancel instanceof Function) {
//             func_if_cancel()
//         }
//     })
// };

// // Now you can call alert("Test") or confirm("Test") and you will see Swal Alerts

// // If you using SweetAlert2 (Swal) and JQuery you can replace all alert and confirm that way.
// // https://limonte.github.io/sweetalert2/
// // TIP: if you aren't using JQuery, use native JavaScript to create extend method. As bellow:
// /*
// function extend(a, b){
//     for(var key in b)
//         if(b.hasOwnProperty(key))
//             a[key] = b[key];
//     return a;
// }
// */

// // keep default js alert to use in specific cases
// window.legacyAlert = window.alert;

// // types alert and confirm: "success", "error", "warning", "info", "question". Default: "warning"
// // overwrite default js alert
// window.alert = function(msg, title, type, params) {
//     var title = (title == null) ? 'Aviso' : title;
//     var type = (type == null) ? 'warning' : type;
//     swal($.extend({
//             title: title,
//             text: msg,
//             type: type
//         }, params || {})
//     );
// };

// // keep default js alert to use in specific cases
// window.legacyConfirm = window.confirm;

// window.confirm = function(msg, title, type, func_if_yes, func_if_cancel, params) {
//     var title = (title == null) ? 'Confirmação' : title;
//     var type = (type == null) ? 'warning' : type;
//     swal($.extend({
//                     title: title,
//                     text: msg,
//                     type: type,
//                     showCancelButton: true,
//                     cancelButtonText: "Cancelar",
//                     confirmButtonText: "Ok",
//                     allowEscapeKey: false,
//                     allowOutsideClick: false
//                 }, params || {})
//     ).then(function(isConfirm) {
//         if (isConfirm && func_if_yes instanceof Function){
//             func_if_yes();
//         }
//     }, function(dismiss) {
//         // dismiss can be 'cancel', 'overlay', 'close', 'timer'
//         if (dismiss === 'cancel' && func_if_cancel instanceof Function) {
//             func_if_cancel()
//         }
//     })
// };

// // Now you can call alert("Test") or confirm("Test") and you will see Swal Alerts

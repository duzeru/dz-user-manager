module.exports = {
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        // directories: {
        //   output: "build"
        // },
        // linux: {
        //   icon: "dist_electron/icons/256x256.png"
        // }
      }
    },
    quasar: {
      importStrategy: "manual",
      rtlSupport: false
    },
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: true
    }
  },
  transpileDependencies: ["quasar"]
};

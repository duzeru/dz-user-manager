# dz-user-manager

# Gerênciador de usuários para sistemas Linux baseados no Debian.


/usr/share/polkit-1/actions/dz-user-manager.policy
/usr/share/applications/dz-user-manager.desktop

sudo chown root chrome-sandbox
sudo chmod 4755 chrome-sandbox

dpkg-deb -b "/home/rener/Documentos/PROJETOS/dz-user-manager/empacotamento" "/home/rener/Documentos/PROJETOS/dz-user-manager/dz-user-manager_0.1.0-alpha.deb.deb"

# dz-user-manager

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
